<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// The auth routes
Auth::routes();

// The home page
Route::get('', 'HomeController@index')->name('home');

Route::get('courses', 'Lesson\LessonController@index')->name('lessons');

Route::group(['middleware' => 'auth'], function(){

	// To show the user the available plans
	Route::get('plans', 'PlanController@index')->name('plans');
	Route::get('plans/{plan}', 'PlanController@show')->name('plan.show');

	// To create a user subscription
	Route::post('subscription', 'SubscriptionController@create')->name('subscription.create');

	// To generate a braintree token
	Route::get('braintree/token', 'BraintreeTokenController@token')->name('braintree.token');
	
	/**
	 * Routes for subscribed users only
	 */
	Route::group(['middleware' => 'subscribed'], function(){
		Route::get('subscription', 'SubscriptionController@index')->name('subscription');
		Route::post('subscription/cancel', 'SubscriptionController@cancel')->name('subscription.cancel');
		Route::post('subscription/resume', 'SubscriptionController@resume')->name('subscription.resume');
	});	

	/**
	 * Routes for pro features only
	 */
	Route::group(['middleware' => 'subscribed.pro'], function(){
		Route::get('lessons/pro', 'Lesson\LessonController@pro')->name('lessons.pro');
	});	

	/**
	 * Customer Middleware for if the signed in user has ever been a customer before
	 */
	Route::group(['middleware' => 'customer'], function(){
		Route::get('invoices', 'Invoices\InvoiceController@index')->name('invoices');
		Route::get('invoices/{invoice_id}', 'Invoices\InvoiceController@show')->name('invoices.show');
	});	

	/**
	 * Routes for the profile pages
	 */
	Route::name('profile')->prefix('profile')->group(function(){
		Route::get('profile/{username}', 'Profile\UserProfileController@getUserProfile');
		Route::post('profile/{username}', 'Profile\UserProfileController@postUserProfileEdit')->name('.edit');
		
		Route::post('profile/{username}/profile-pic/upload', 'Profile\UserProfileImagesController@postUserProfilePicUpload')->name('.pic');
		Route::post('profile/{username}/banner/upload', 'Profile\UserProfileImagesController@postUserProfileBannerUpload')->name('.banner');
	});

	/**
	 * Routes for the admin area
	 * Admin auth middleware to be added
	 */
	Route::group(['middleware' => 'auth'], function(){
		Route::get('q-admin', 'Admin\DashboardController@index')->name('admin');

		Route::name('admin.')->prefix('q-admin')->group(function(){
			
			/*===========================================
			=            The admin plan area            =
			===========================================*/
			
			// A list of all the plans, paginated
			Route::get('plans', 'Admin\Plans\PlanController@index')->name('plans');

			// For editing plans
			Route::get('plan/edit/{planId}', 'Admin\Plans\PlanController@getEdit')->name('plan.edit');
			Route::post('plan/edit/{planId}', 'Admin\Plans\PlanController@postEdit');

			// For adding plans
			Route::get('plan/add', 'Admin\Plans\PlanController@getAdd')->name('plan.add');
			Route::post('plan/add', 'Admin\Plans\PlanController@postAdd');
			
			// For adding plans
			Route::post('plan/delete/{planId}', 'Admin\Plans\PlanController@postDelete')->name('plan.delete');

			/*=====  End of The admin plan area  ======*/
			
			/*=============================================
			=            The admin course area            =
			=============================================*/
			
			// A list of all the courses
			Route::get('courses', 'Admin\Courses\CourseController@index')->name('courses');

			// The add course page
			Route::get('course/add', 'Admin\Courses\AddCourseController@index')->name('course.add');
			Route::post('course/add', 'Admin\Courses\AddCourseController@addCourse');

			// The edit course page
			Route::get('course/edit/{course}', 'Admin\Courses\EditCourseController@index')->name('course.edit');
			Route::post('course/edit/{course}', 'Admin\Courses\EditCourseController@EditCourse');
			
			/*=====  End of The admin course area  ======*/
			
		});
	});

});