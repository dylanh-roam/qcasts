<?php

namespace Qcasts\Models;

use Illuminate\Database\Eloquent\Model;
use Qcasts\Models\Course;

class Video extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'video_url',
        'course_id',
        'code_download',
        'code_repo',
        'paid',
    ];

    /**
     * Allows the 'slug' to be used as the identifier
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
