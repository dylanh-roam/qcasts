<?php

namespace Qcasts\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'braintree_plan',
        'cost',
        'description',
    ];

    /**
     * Allows the 'slug' to be used as the identifier
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /*====================================
    =            Plan helpers            =
    ====================================*/
    
    /**
     * Get's the plan cost in money format
     */
    public function getCost($format = true){
        if($format){
            return number_format($this->cost,2);
        }
        return $this->cost;
    }
    
    /*=====  End of Plan helpers  ======*/
    
    

}