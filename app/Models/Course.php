<?php

namespace Qcasts\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'short_description',
        'long_description',
        'paid',
        'published',
    ];

    /**
     * Allows the 'slug' to be used as the identifier
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
