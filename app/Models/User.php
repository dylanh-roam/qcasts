<?php

namespace Qcasts\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'email', 
        'password',
        'first_name',
        'last_name',
        'activation_token',
        'active',
        'braintree_id',
        'paypal_email',
        'card_last_four',
        'card_brand',
        'trial_ends_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'braintree_id',
        'paypal_email',
        'card_last_four',
        'card_brand',
        'trial_ends_at',
        'activation_token',
        'active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Define the route key
    public function getRouteKeyName()
    {
        return 'username';
    }

    /*============================================
    =            user helper methods            =
    ============================================*/

    // Gets the user's full name or first name
    public function getName()
    {
        if($this->first_name && $this->last_name){
            return "{$this->first_name} {$this->last_name}";
        }

        if($this->first_name){
            return "{$this->first_name}";
        }

        return null;
    }

    // Gets the user's name or their username
    public function getUsername(){
        return $this->username;
    }

    // Gets the user's name or their username
    public function getNameOrUsername(){
        return $this->getName() ?: $this->username;
    }

    // Gets only the user's first name or username
    public function getFirstNameOrUsername()
    {
        return $this->first_name ?: $this->username;
    }

    // Gets only the user's first name
    public function getFirstName()
    {
        return $this->first_name;
    }

    // Gets only the user's last name
    public function getLastName()
    {
        return $this->last_name;
    }

    // Get the user's profile picture or return a default one
    public function getProfilePicture(){
        return $this->profile_image_url ?: '/images/defaults/profile-default.jpg';
    }

    // Get the user's profile picture or return a default one
    public function getProfileBanner(){
        return $this->banner_url ?: '/images/defaults/profile-background-default.jpg';
    }

    // Get the user's Bio
    public function getBio(){
        return $this->bio;
    }

    // Get created date
    public function getMemberSince(){
        return $this->created_at->diffForHumans();
    }

    /*=====  End of user helper methods  ======*/
    

    /*===========================================
    =            subscription checks            =
    ===========================================*/
    
    // Check if the user has any subscription
    public function hasSubscription(){
        return $this->subscribed('main');
    }

    // Check if the user has a Pro subscription
    public function hasProSubscription(){
        return $this->subscribedToPlan('pro','main');
    }
    
    // Check if the user has had any subscription in the past
    public function hasBeenCustomer(){
        return $this->braintree_id !== null;
    }

    /*=====  End of subscription checks  ======*/
}