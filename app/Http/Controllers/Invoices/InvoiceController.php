<?php

namespace Qcasts\Http\Controllers\Invoices;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
    	return view('invoices.invoices')->with([
    		'invoices' => $request->user()->invoices(),
    	]);
    }

    public function show($invoice_id, Request $request)
    {
    	return $request->user()->downloadInvoice($invoice_id,[
    		'vendor' => 'Jozituts Pty Ltd',
    		'product' => 'Online Subscription',
    	]);
    }
}