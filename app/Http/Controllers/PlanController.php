<?php

namespace Qcasts\Http\Controllers;

use Illuminate\Http\Request;
use Qcasts\Models\Plan;

class PlanController extends Controller
{
    /**
     * Gets all the plans to display to the user
     */
    public function index()
    {
    	return view('plans.plans')->with([
    		'plans'	=> Plan::get(),
    	]);
    }

    /**
     * Shows an indiviual plan to the user
     */
    public function show(Request $request,Plan $plan)
    {
        if($request->user()->subscribedToPlan($plan->braintree_plan, 'main')){
            return redirect()->route('plans');
        }

    	return view('plans.plan-purchase')->with([
    		'plan'	=> $plan,
    	]);
    }
}