<?php

namespace Qcasts\Http\Controllers\Lesson;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;

class LessonController extends Controller
{
    public function index()
    {
    	return 'The lessons';
    }

    public function pro()
    {
    	return 'The Pro lessons';
    }
}