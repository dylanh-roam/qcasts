<?php

namespace Qcasts\Http\Controllers\Profile;

use Qcasts\Models\User;
use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;

class UserProfileController extends Controller
{   
    /**
     * Get the user's profile
     */
    public function getUserProfile($username){
        return view('profile.profile')->with([
            'user' => User::where('username',$username)->firstOrFail(),
        ]);
    }

    /**
     * Handle the data submitted by the user
     */
    public function postUserProfileEdit(Request $request,$username){
    	
    	$user = User::where('username',$username)->firstOrFail();

        $user->first_name   = $request->input('first_name');
        $user->last_name    = $request->input('last_name');
    	$user->bio          = $request->input('bio');
    	$user->twitter 		= $request->input('twitter');
    	$user->facebook 	= $request->input('facebook');
    	$user->youtube		= $request->input('youtube');
    	$user->github		= $request->input('github');
    	$user->bitbucket 	= $request->input('bitbucket');

    	$user->save();

    	return redirect()->route('profile',$user->username);
    }
}
