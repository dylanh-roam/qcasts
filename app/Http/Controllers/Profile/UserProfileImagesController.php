<?php

namespace Qcasts\Http\Controllers\Profile;

use Qcasts\Models\User;
use Illuminate\Http\Request;
use Qcasts\Http\Requests\Profile\ProfilePictureRequest;
use Qcasts\Http\Requests\Profile\ProfileBannerRequest;
use Qcasts\Http\Controllers\Controller;

use File;
use Image;

class UserProfileImagesController extends Controller
{
    protected $user;

    /**
     * Handle the uploaded profile image
     */
    public function postUserProfilePicUpload(ProfilePictureRequest $request,$username){
        $this->user = $request->user();

        $this->saveProfilePic($request->file('profile'));

        return redirect()->route('profile',$username);
    }

    /**
     * Handle the uploaded banner image
     */
    public function postUserProfileBannerUpload(ProfileBannerRequest $request,$username){
        $this->user = $request->user();
        $this->saveBanner($request->file('banner'));

        return redirect()->route('profile',$username);
    }

    /**
     * Save the profile image
     */
    protected function saveProfilePic($profilePic)
    {
        $image_resize = Image::make($profilePic);
        $image_resize->fit(250, 250);
        $file_path = '/images/profile/';
        $name = time(). $this->user->username .'.'.$profilePic->getClientOriginalExtension();

        $image_resize->save(public_path($file_path . $name));
        
        $this->user->profile_image_url = $file_path . $name;
        $this->user->save(); 
    }

     /**
     * Save the banner image
     */
    protected function saveBanner($banner)
    {
        $image_resize = Image::make($banner);
        $image_resize->fit(2160, 300);
        $file_path = '/images/banner/';
        $name = time(). $this->user->username .'.'.$banner->getClientOriginalExtension();

        $image_resize->save(public_path($file_path . $name));
        
        $this->user->banner_url = $file_path . $name;
        $this->user->save(); 
    }
}
