<?php

namespace Qcasts\Http\Controllers\Admin\Courses;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;
use Qcasts\Models\Course;
use Qcasts\Models\Video;

class EditCourseController extends Controller
{
    /**
     * Returns the admin 'edit course' view
     */
    public function index($course){
    	return view('admin.courses.edit')->with([
            'course' => Course::find($course),
    		'videos' => Video::where('course_id',$course)->get(),
    	]);
    }

    /**
     * Adds the submitted course data to the DB
     */
    public function editCourse(Request $request, $courseId){
    	$course = Course::find($courseId);

    	// Validate the name and slug
        $request->validate([
            'name'              => 'required',
        ]);

        // Create a slug if one wasn't filled in
        if($request->input('slug') == ''){
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->input('name'))));
        }
        else{
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->input('slug'))));
        }

        // Create the course
        $course->update([
            'name'              => $request->input('name'),
            'published'         => $request->input('published') ? true : false,
            'paid'        		=> $request->input('paid') ? true : false,
            'slug'              => $slug,
            'short_description' => $request->input('short_description'),
            'long_description'  => $request->input('long_description'),
        ]);

        return redirect()->route('admin.course.edit',$course->id);
    }
}
