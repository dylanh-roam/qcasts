<?php

namespace Qcasts\Http\Controllers\Admin\Courses;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;
use Qcasts\Models\Course;

class CourseController extends Controller
{
    /**
     * Gets a paginated list of all courses
     */
    public function index(){
    	return view('admin.courses.courses')->with([
    		'courses'	=> Course::paginate(15),
    	]);
    }
    
}
