<?php

namespace Qcasts\Http\Controllers\Admin\Courses;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;
use Qcasts\Models\Course;

class AddCourseController extends Controller
{
    /**
     * Returns the admin 'add course' view
     */
    public function index(){
    	return view('admin.courses.add');
    }

    /**
     * Adds the submitted course data to the DB
     */
    public function addCourse(Request $request){
        
        // Validate the name and slug
        $request->validate([
            'name'              => 'required',
            'slug'              => 'unique:courses,slug',
        ]);

        // Create a slug if one wasn't filled in
        if($request->input('slug') == ''){
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->input('name'))));
        }
        else{
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $request->input('slug'))));
        }

        // Create the course
        $course = Course::create([
            'name'              => $request->input('name'),
            'slug'              => $slug,
            'short_description' => $request->input('short_description'),
            'long_description'  => $request->input('long_description'),
        ]);

        return redirect()->route('admin.course.edit',$course->id);
    }


}
