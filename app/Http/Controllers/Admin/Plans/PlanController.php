<?php

namespace Qcasts\Http\Controllers\Admin\Plans;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;
use Qcasts\Models\Plan;

class PlanController extends Controller
{
    /**
	 * Returns a list of plans to the admin dashboard
	 */
    public function index(Request $request)
    {
    	return view('admin.plans.plans')->with([
    		'plans'	=> Plan::paginate(15),
    	]);
    }

    /**
	 * Returns a plan in the edit plan view
	 */
    public function getEdit(Request $request, $planId)
    {
    	return view('admin.plans.edit')->with([
    		'plan'	=> Plan::findOrFail($planId),
    	]);
    }

    /**
	 * Saves an edited plan
	 */
    public function postEdit(Request $request, $planId)
    {
    	$plan = Plan::findOrFail($planId);

    	$plan->update([
    		'name'				=> $request->input('name'),
    		'slug'				=> $request->input('slug'),
    		'braintree_plan'	=> $request->input('braintree_plan'),
    		'cost'				=> $request->input('cost'),
    		'description'		=> $request->input('description'),
    	]);

    	return redirect()->route('admin.plan.edit',$plan->id)->with([
    		'info'	=> 'Plan saved successfully',
    	]);
    }

    /**
	 * Returns a plan in the add plan view
	 */
    public function getAdd(Request $request)
    {
    	return view('admin.plans.add');
    }

    /**
	 * Returns a plan in the add plan view
	 */
    public function postAdd(Request $request)
    {
    	$plan = Plan::create([
    		'name'				=> $request->input('name'),
    		'slug'				=> $request->input('slug'),
    		'braintree_plan'	=> $request->input('braintree_plan'),
    		'cost'				=> $request->input('cost'),
    		'description'		=> $request->input('description'),
    	]);

    	return redirect()->route('admin.plan.edit',$plan->id)->with([
    		'info'	=> 'Plan added successfully',
    	]);
    }

    /**
     * Deletes a plan
     */
    public function postDelete(Request $request, $planId)
    {
        $plan = Plan::findOrFail($planId);

        $plan->delete();

        return redirect()->route('admin.plans')->with([
            'info'  => 'Plan deleted successfully',
        ]);
    }
}
