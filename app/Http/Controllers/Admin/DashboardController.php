<?php

namespace Qcasts\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Qcasts\Http\Controllers\Controller;
use Qcasts\Models\User;

class DashboardController extends Controller
{
	/**
	 * Returns the admin dashboard view
	 */
    public function index(Request $request)
    {
    	return view('admin.dashboard')->with([
    		'user'		=> User::find($request->user()->id),
    	]);
    }
}
