<?php

namespace Qcasts\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Cashier\Billable;
use Qcasts\Models\Plan;

class SubscriptionController extends Controller
{
    /**
     * Show the user the subscription manager page
     */
    public function index(Request $request)
    {
        return view('subscription.subscription-manager');
    }

    /**
     * Create a new subscription
     */
    public function create(Request $request)
    {
    	$plan = Plan::findOrFail($request->plan);

    	if($request->user()->subscribedToPlan($plan->braintree_plan, 'main')){
            return redirect()->route('plans');
        }

        if(!$request->user()->subscribed('main')){
            $request->user()->newSubscription('main',$plan->braintree_plan)->create($request->payment_method_nonce);
        }
        else{
        	$request->user()->subscription('main')->swap($plan->braintree_plan);
        }

    	return redirect()->route('home');
    }

    /**
     * Cancel a subscription
     */
    public function cancel(Request $request)
    {
        $request->user()->subscription('main')->cancel();
        return redirect()->route('subscription')->with([
            'info' => 'Your subscription has been cancelled.',
        ]);
    }

    /**
     * Resume a subscription that may have been cancelled by accident
     */
    public function resume(Request $request)
    {
        $request->user()->subscription('main')->resume();
        return redirect()->route('subscription')->with([
            'info' => 'Your subscription has been resumed.',
        ]);
    }
}

