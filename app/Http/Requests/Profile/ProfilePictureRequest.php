<?php

namespace Qcasts\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class ProfilePictureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile' => 'required|mimes:jpeg,jpg,png',
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'profile.required' => 'Please select a profile image to upload',
            'profile.mimes'  => 'The profile image must be a jpeg, jpg or png file.',
        ];
    }
}
