<?php

namespace Qcasts\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class ProfileBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'banner' => 'required|mimes:jpeg,jpg,png',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'banner.required' => 'Please select a banner image to upload',
            'banner.mimes'  => 'The banner image must be a jpeg, jpg or png file.',
        ];
    }
}
