<?php

namespace Qcasts\Http\Middleware;

use Closure;

class SubscribedPro
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->hasProSubscription()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->route('plans')->with('status','Please purchase a pro subscription to view this lesson');
            }
        }

        return $next($request);
    }
}