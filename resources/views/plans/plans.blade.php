@extends('templates.default')

@section('content')
<section class="uk-section">
    <div class="uk-grid" uk-grid>

        <aside class="uk-width-1-4@s">
            @include('templates.partials.account_nav')
        </aside>

        <main class="uk-width-3-4@s">
            <h1>Plans</h1>

            <div class="uk-child-width-1-2@s uk-grid-match" uk-grid>
                @foreach($plans as $plan)
                    <div>
                        <div class="uk-card uk-card-default">
                            <div class="uk-card-body">
                                <h3 class="uk-card-title uk-margin-remove">{{$plan->name}}</h3>
                                <h4 class="uk-margin-remove">{{$plan->getCost()}}</h4>
                                @if($plan->description)
                                    <p>
                                        {{$plan->description}}
                                    </p>
                                @endif
                            </div>
                            <div class="uk-card-footer">
                                @if(!Auth::user()->subscribedToPlan($plan->braintree_plan, 'main'))
                                    <a href="{{route('plan.show', $plan->slug)}}" class="uk-button uk-button-primary">
                                        Choose
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            
        </main>
    </div>
</section>
@endsection