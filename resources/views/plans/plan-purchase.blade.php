@extends('templates.default')

@section('content')
<main class="uk-section">
    <form action="{{route('subscription.create')}}" method="post">
        @csrf
        <input type="hidden" name="plan" value="{{$plan->id}}">
        <div id="dropin-container"></div>
        <hr>
        <button id="pay-button" type="submit" class="uk-button uk-button-primary" hidden>Pay</button>
    </form>
</main>
@endsection

@section('scripts.footer')
    <script src="https://js.braintreegateway.com/js/braintree-2.32.1.min.js"></script>
    <script type="text/javascript">
        
        $.ajax({
            url: ' {{route('braintree.token')}} ',
            success: function(response){
                braintree.setup(response.data.token, 'dropin', {
                    container: 'dropin-container',
                    onReady: function(){
                        $('#pay-button').removeAttr('hidden');
                    }
                });
            },
        });
        
    </script>
@endsection