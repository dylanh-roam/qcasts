@extends('templates.default')

@section('content')
<section class="uk-section">
  <div class="uk-grid" uk-grid>

    <aside class="uk-width-1-4@m uk-width-1-3@s">
        @include('templates.partials.account_nav')
    </aside>

    <main class="uk-width-3-4@m uk-width-2-3@s">
      <table class="uk-table uk-table-striped">
         <thead>
             <tr>
                 <th>Date</th>
                 <th>Ammount</th>
                 <th></th>
             </tr>
         </thead>
         <tbody>
             @foreach($invoices as $invoice)
                  <tr>
                     <td>{{$invoice->date()->toDateTimeString()}}</td>
                     <td>{{$invoice->total()}}</td>
                     <td><a href="{{route('invoices.show',['invoice_id'=>$invoice->id])}}">Download</a></td>
                  </tr>
             @endforeach
         </tbody>
      </table>
    </main>

  </div>
</section>
@endsection
