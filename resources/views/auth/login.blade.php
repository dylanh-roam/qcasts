@extends('templates.default')

@section('content')
<form method="POST" class="uk-form-stacked uk-width-large uk-align-center" action="{{ route('login') }}">
    @csrf

    <div class="uk-margin">
        <label for="email">Email Address</label>

        <div>
            <input 
                id="email" 
                type="email" 
                class="uk-input{{ $errors->has('email') ? ' uk-form-danger' : '' }}" 
                name="email" 
                value="{{ old('email') }}" 
                required
                placeholder="Email Address" 
            >

            @if ($errors->has('email'))
                <span class="uk-text-danger">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label for="password">Password</label>

        <div>
            <input 
                id="password" 
                type="password" 
                class="uk-input{{ $errors->has('password') ? ' uk-form-danger' : '' }}" 
                name="password" 
                required
                placeholder="Password" 
            >

            @if ($errors->has('password'))
                <span class="uk-text-danger">
                    {{ $errors->first('password') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label>
            <input type="checkbox" class="uk-checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
        </label>
    </div>

    <div class="uk-margin">
        <button type="submit" class="uk-button uk-button-primary uk-width-1-1">
            Login
        </button>
    </div>
    <div class="uk-margin">
        <a class="uk-button uk-button-link" href="{{ route('password.request') }}">
            {{ __('Forgot Your Password?') }}
        </a>
    </div>
</form>
@endsection
