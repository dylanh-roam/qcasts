@extends('templates.default')

@section('content')
<form method="POST" class="uk-form-stacked uk-width-large uk-align-center" action="{{ route('password.email') }}">
    @csrf

    <div class="uk-margin">
        <label for="email">Email Address</label>

        <div>
            <input 
                id="email" 
                type="email" 
                class="uk-input{{ $errors->has('email') ? ' uk-form-danger' : '' }}" 
                name="email" 
                value="{{ old('email') }}" 
                placeholder="Email Address"
                required
            >

            @if ($errors->has('email'))
                <span class="uk-text-danger">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <button type="submit" class="uk-button uk-button-primary uk-width-1-1">
            {{ __('Send Password Reset Link') }}
        </button>
    </div>
</form>
@endsection
