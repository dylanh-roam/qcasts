@extends('templates.default')

@section('content')
<form method="POST" class="uk-form-stacked uk-width-large uk-align-center" action="{{ route('password.request') }}">
    @csrf

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="uk-margin">
        <label for="email">Email Address</label>

        <div>
            <input 
                id="email" 
                type="email" 
                class="uk-input{{ $errors->has('email') ? ' uk-form-danger' : '' }}" 
                name="email" 
                value="{{ old('email') }}" 
                required
                placeholder="Email Address"
            >

            @if ($errors->has('email'))
                <span class="uk-text-danger">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label for="password">Password</label>

        <div>
            <input 
                id="password" 
                type="password" 
                class="uk-input{{ $errors->has('password') ? ' uk-form-danger' : '' }}" 
                name="password" 
                placeholder="Password" 
                required
            >

            @if ($errors->has('password'))
                <span class="uk-text-danger">
                    {{ $errors->first('password') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label for="password-confirm">Confirm Password</label>

        <div>
            <input 
                id="password-confirm" 
                type="password" 
                class="uk-input" 
                name="password_confirmation" 
                required
                placeholder="Confirm Password" 
            >
        </div>
    </div>

    <div class="form-group row mb-0">
        <button type="submit" class="uk-button uk-button-primary uk-width-1-1">
            {{ __('Reset Password') }}
        </button>
    </div>
</form>
@endsection
