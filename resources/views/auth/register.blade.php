@extends('templates.default')

@section('content')
<form method="POST" class="uk-form-stacked uk-width-large uk-align-center" action="{{ route('register') }}">
    @csrf

    <div class="uk-margin">
        <label for="username">Username</label>

        <div>
            <input 
                id="name" 
                type="text" 
                class="uk-input{{ $errors->has('username') ? ' uk-form-danger' : '' }}" 
                name="username" 
                placeholder="Username"
                value="{{ old('username') }}" 
                required 
                autofocus
            >

            @if ($errors->has('username'))
                <span class="uk-text-danger">
                    {{ $errors->first('username') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label for="email">Email Address</label>

        <div>
            <input 
                id="email" 
                type="email" 
                class="uk-input{{ $errors->has('email') ? ' uk-form-danger' : '' }}" 
                name="email" 
                placeholder="Email Address" 
                value="{{ old('email') }}" 
                required
            >

            @if ($errors->has('email'))
                <span class="uk-text-danger">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label for="password">Password</label>

        <div>
            <input 
                id="password" 
                type="password" 
                class="uk-input{{ $errors->has('password') ? ' uk-form-danger' : '' }}" 
                name="password" 
                placeholder="Password" 
                required
            >

            @if ($errors->has('password'))
                <span class="uk-text-danger">
                    {{ $errors->first('password') }}
                </span>
            @endif
        </div>
    </div>

    <div class="uk-margin">
        <label for="password-confirm">Confirm Password</label>

        <div>
            <input 
                id="password-confirm" 
                type="password" 
                class="uk-input" 
                name="password_confirmation" 
                placeholder="Confirm Password" 
                required
            >
        </div>
    </div>

    <div class="uk-margin-top">
        <button type="submit" class="uk-button uk-button-primary uk-width-1-1">
            Register
        </button>
    </div>
</form>
@endsection
