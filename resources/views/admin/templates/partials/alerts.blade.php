@if (session('info'))
    <div class="uk-alert uk-alert-primary" uk-alert>
    	<a class="uk-alert-close" uk-close></a>
        {{ session('info') }}
    </div>
@endif
@if (session('error'))
    <div class="uk-alert uk-alert-danger" uk-alert>
    	<a class="uk-alert-close" uk-close></a>
        {{ session('error') }}
    </div>
@endif
@if (session('success'))
    <div class="uk-alert uk-alert-success" uk-alert>
    	<a class="uk-alert-close" uk-close></a>
        {{ session('success') }}
    </div>
@endif
@if (session('warning'))
    <div class="uk-alert uk-alert-warning" uk-alert>
    	<a class="uk-alert-close" uk-close></a>
        {{ session('warning') }}
    </div>
@endif