<div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; top: 200" class="uk-background-dark">

    <nav class="uk-navbar-transparent">
        <div class="uk-padding uk-padding-remove-vertical">
            <div uk-navbar="mode: click; dropbar: true;">

                <div class="uk-navbar-left">
			        <a class="uk-navbar-item uk-logo uk-padding-remove-left" href="{{route('admin')}}">
						<img src="{{asset('/images/qcasts-logo-white.png')}}" alt="qcasts" width="150">
			        </a>
			    </div>

			    <div class="uk-navbar-right">
			    	<ul class="uk-navbar-nav uk-visible@m">
			    		
			        </ul>
			        <a class="uk-navbar-toggle uk-hidden@m" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-push"></a>
			    </div>

            </div>
        </div>
    </nav>

</div>