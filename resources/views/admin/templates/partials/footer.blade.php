<footer id="tm-footer" class="uk-section">
	<div class="uk-container uk-text-center">
		<p><small>&copy; {{ date("Y") }} Qcasts Limited. All rights reserved.</small></p>
	</div>
</footer>