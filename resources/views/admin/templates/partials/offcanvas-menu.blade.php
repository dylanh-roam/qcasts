<div id="offcanvas-push" uk-offcanvas="mode: push; overlay: true;">
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <ul class="uk-nav uk-nav-default">
        	@include('admin.templates.partials.nav')
	    </ul>
    </div>
</div>