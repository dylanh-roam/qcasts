<ul class="uk-nav uk-nav-default">
	<li class="uk-nav-header">
    	Courses
	</li>
	<li>
		<a href="{{route('admin.course.add')}}">Add Course</a>
	</li>
	<li>
		<a href="{{route('admin.courses')}}">All Courses</a>
	</li>
	<li class="uk-nav-header">
    	Plans
	</li>
	<li>
		<a href="{{route('admin.plan.add')}}">Add Plan</a>
	</li>
	<li>
		<a href="{{route('admin.plans')}}">All Plans</a>
	</li>
</ul>
