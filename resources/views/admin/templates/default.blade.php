<!DOCTYPE html>
<html>
@include('admin.templates.partials.head')
<body>

<div class="uk-offcanvas-content">
	@include('admin.templates.partials.header')
	<div class="uk-grid uk-grid-collapse" uk-grid>
		<div class="uk-width-medium@m uk-background-dark uk-visible@m uk-light">
			<div class="uk-padding">
				<nav class="">
					@include('admin.templates.partials.nav')
				</nav>
			</div>
		</div>
		<div class="uk-width-expand">
			<main class="uk-padding">
				@include('admin.templates.partials.alerts')
				@yield('content')
			</main>
			@include('admin.templates.partials.footer')
		</div>
	</div>
</div>
@include('admin.templates.partials.offcanvas-menu')

@yield('scripts.footer')
</body>
</html>