@extends('admin.templates.default')

@section('content')

<h1>
	Add Plan
</h1>

<form class="uk-form uk-form-horizontal" action="" method="POST">
	@include('admin.plans.partials.form')
</form>

@endsection
