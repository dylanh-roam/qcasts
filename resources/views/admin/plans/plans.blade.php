@extends('admin.templates.default')

@section('content')

<h1>
	Plans
</h1>

<table class="uk-table uk-table-striped uk-table-middle uk-table-responsive">
	<thead>
		<tr>
			<th>Plan</th>
			<th>Slug</th>
			<th>Price</th>
			<th class="uk-table-shrink"></th>
		</tr>
	</thead>
	<tbody>
		@foreach($plans as $plan)
			<tr>
				<td>{{$plan->name}}</td>
				<td>{{$plan->slug}}</td>
				<td><i class="fas fa-dollar-sign"></i> {{$plan->getCost()}}</td>
				<td>
					<a href="{{route('admin.plan.edit',$plan->id)}}" class="uk-button uk-button-primary">
						Edit
					</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{{$plans->render()}}

@endsection
