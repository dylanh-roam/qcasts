@csrf

<div class="uk-margin">
	<label for="name">Plan Name</label>
	
	<div>
        <input 
            id="name" 
            type="text" 
            class="uk-input{{ $errors->has('name') ? ' uk-form-danger' : '' }}" 
            name="name" 
            placeholder="Plan Name" 
            required
            value="{{ old('name') || $errors->has('name') || !isset($plan)
        		? old('name')
        		: $plan->name
        	}}"
        >

        @if ($errors->has('name'))
            <span class="uk-text-danger">
                {{ $errors->first('name') }}
            </span>
        @endif
    </div>
</div>

<div class="uk-margin">
	<label for="slug">Plan Slug</label>
	
	<div>
        <input 
            id="slug" 
            type="text" 
            class="uk-input{{ $errors->has('slug') ? ' uk-form-danger' : '' }}" 
            name="slug" 
            placeholder="Plan Name" 
            required
            value="{{ old('slug') || $errors->has('slug') || !isset($plan)
        		? old('slug')
        		: $plan->slug
        	}}"
        >

        @if ($errors->has('slug'))
            <span class="uk-text-danger">
                {{ $errors->first('slug') }}
            </span>
        @endif
    </div>
</div>

<div class="uk-margin">
	<label for="braintree_plan">BrainTree Plan Id</label>
	
	<div>
        <input 
            id="braintree_plan" 
            type="text" 
            class="uk-input{{ $errors->has('braintree_plan') ? ' uk-form-danger' : '' }}" 
            name="braintree_plan" 
            placeholder="Plan Name" 
            required
            value="{{ old('braintree_plan') || $errors->has('slug') || !isset($plan)
        		? old('braintree_plan')
        		: $plan->braintree_plan
        	}}"
        >

        @if ($errors->has('braintree_plan'))
            <span class="uk-text-danger">
                {{ $errors->first('braintree_plan') }}
            </span>
        @endif
    </div>
</div>

<div class="uk-margin">
	<label for="cost">Cost</label>
	
	<div>
		<div class="uk-inline uk-width-1-1">
			<span class="uk-form-icon">
				<i class="fas fa-dollar-sign"></i>
			</span>
            <input 
                id="cost" 
                type="number" 
                class="uk-input{{ $errors->has('braintree_plan') ? ' uk-form-danger' : '' }}" 
                name="cost" 
                placeholder="Plan Cost" 
                required
                value="{{ old('cost') || $errors->has('cost') || !isset($plan)
	        		? old('cost')
	        		: $plan->cost
	        	}}"
            >

            @if ($errors->has('cost'))
                <span class="uk-text-danger">
                    {{ $errors->first('cost') }}
                </span>
            @endif
		</div>
    </div>
</div>

<div class="uk-margin">
	<label for="description">Description</label>
	
	<div>
        <textarea
            id="description" 
            type="text" 
            class="uk-textarea{{ $errors->has('description') ? ' uk-form-danger' : '' }}" 
            name="description" 
            placeholder="Plan Name"
            >{{ old('description') || $errors->has('description') || !isset($plan)
        		? old('description')
        		: $plan->description
        	}}</textarea> 

        @if ($errors->has('description'))
            <span class="uk-text-danger">
                {{ $errors->first('description') }}
            </span>
        @endif
    </div>
</div>

<div class="uk-margin-top">		
	<div>
        <button class="uk-button uk-button-primary" type="submit">
        	Save
        </button>
    </div>
</div>