@extends('admin.templates.default')

@section('content')

<h1>
	Edit Plan
</h1>

<form class="uk-form uk-form-horizontal" action="" method="POST">
	@include('admin.plans.partials.form')
</form>

<form class="uk-form uk-form-horizontal uk-margin" action="{{route('admin.plan.delete',$plan->id)}}" method="POST">
	@csrf
	<button class="uk-button uk-button-danger" type="submit">
		Delete
	</button>
</form>

@endsection
