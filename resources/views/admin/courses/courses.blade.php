@extends('admin.templates.default')

@section('content')

<h1>
	Courses
</h1>

<table class="uk-table uk-table-striped uk-table-responsive uk-table-middle">
	<thead>
		<tr>
			<th>Course Name</th>
			<th>Course Slug</th>
			<th>Published</th>
			<th>Paid</th>
			<th class="uk-table-shrink"></th>
		</tr>
	</thead>
	<tbody>
		@foreach($courses as $course)
			<tr>
				<td>{{$course->name}}</td>
				<td>{{$course->slug}}</td>
				<td>{{$course->published}}</td>
				<td>{{$course->paid}}</td>
				<td>
					<a href="{{route('admin.course.edit',$course->id)}}" class="uk-button uk-button-primary">
						Edit
					</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@endsection
