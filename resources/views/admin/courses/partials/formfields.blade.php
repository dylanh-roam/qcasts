<div class="uk-margin">
	
	@if(isset($course))
	<div class="uk-margin">
		<input 
			type="checkbox" 
			name="published"
			id="published"
			class="uk-checkbox{{ $errors->has('checkbox') ? ' uk-form-danger' : '' }}"
			value="1" 
	        @if(isset($course->published))
				@if(old('published') || $course->published)
					checked
				@endif
	        @endif
		> <label for="published">Published</label>
	</div>
	@endif

	@if(isset($course))
	<div class="uk-margin">
		<input 
			type="checkbox" 
			name="paid"
			id="paid"
			class="uk-checkbox{{ $errors->has('checkbox') ? ' uk-form-danger' : '' }}"
			value="1" 
	        @if(isset($course))
				@if(old('paid') || $course->paid)
					checked
				@endif
	        @endif
		> <label for="paid">Paid Course</label>
	</div>
	@endif

	<label for="name">Course Name</label>
	<div>
		<input 
			type="text" 
			name="name"
			id="name"
			placeholder="Course Name"
			class="uk-input{{ $errors->has('name') ? ' uk-form-danger' : '' }}" 
			required 
			value="{{ old('name') || $errors->has('name') || !isset($course)
        		? old('name')
        		: $course->name
        	}}"
		>
	</div>
</div>

<div class="uk-margin">
	<label for="slug">Course Slug</label>
	<div>
		<input 
			type="text" 
			name="slug"
			id="slug"
			placeholder="Course Slug"
			class="uk-input{{ $errors->has('slug') ? ' uk-form-danger' : '' }}" 
			value="{{ old('slug') || $errors->has('slug') || !isset($course)
        		? old('slug')
        		: $course->slug
        	}}"
		>
	</div>
</div>

<div class="uk-margin">
	<label for="short_description">Short Description</label>
	<div>
		<textarea
			type="text" 
			name="short_description"
			id="short_description"
			class="uk-textarea{{ $errors->has('short_description') ? ' uk-form-danger' : '' }}" 
			placeholder="Short Description" 
			>{{ old('short_description') || $errors->has('short_description') || !isset($course)
        		? old('short_description')
        		: $course->short_description
        	}}</textarea> 
	</div>
</div>

<div class="uk-margin">
	<label for="long_description">Long Description</label>
	<div>
		<textarea
			type="text" 
			name="long_description"
			id="long_description"
			class="uk-textarea{{ $errors->has('long_description') ? ' uk-form-danger' : '' }}" 
			placeholder="Long Description" 
			>{{ old('long_description') || $errors->has('long_description') || !isset($course)
        		? old('long_description')
        		: $course->long_description
        	}}</textarea> 
	</div>
</div>

<div class="uk-margin">
	<button type="submit" class="uk-button uk-button-primary">
		Save Course
	</button>
</div>

@csrf