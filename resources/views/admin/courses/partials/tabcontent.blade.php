<ul uk-tab class="uk-tab">
    <li class="uk-active"><a href="#">Course Settings</a></li>
    <li><a href="#">Course Videos</a></li>
</ul>
<ul class="uk-switcher">
    <li>
    	<form class="uk-form uk-form-horizontal" action="" method="POST">
    		@include('admin.courses.partials.formfields')
    	</form>
    </li>
    <li>
    	@if(isset($course))

    	@else
			<p>This course must be saved first.</p>
    	@endif
    </li>
</ul>