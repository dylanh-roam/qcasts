@extends('admin.templates.default')

@section('content')

<h1>
	Edit Course
</h1>

@include('admin.courses.partials.tabcontent')

@endsection