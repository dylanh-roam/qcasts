<section class="uk-section uk-background-light">
	<div class="uk-container uk-text-center">
		<h1>Learn the languages of the web</h1>

		<div class="uk-grid uk-child-width-1-4@m uk-child-width-1-2@s" uk-grid>
			
			<div>
				<div class="uk-card uk-card-body">
					<div>
						<i class="fab fa-php fa-7x"></i>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, expedita quas ex est! Nulla dignissimos nam quam natus ab in veniam quaerat doloremque maxime molestias aperiam, libero, omnis, facere nesciunt.
					</p>
					<a href="#" class="uk-button uk-button-primary">
						GO!
					</a>
				</div>
			</div>

			<div>
				<div class="uk-card uk-card-body">
					<div>
						<i class="fab fa-html5 fa-7x"></i>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, expedita quas ex est! Nulla dignissimos nam quam natus ab in veniam quaerat doloremque maxime molestias aperiam, libero, omnis, facere nesciunt.
					</p>
					<a href="#" class="uk-button uk-button-secondary">
						GO!
					</a>
				</div>
			</div>

			<div>
				<div class="uk-card uk-card-body">
					<div>
						<i class="fab fa-laravel fa-7x"></i>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, expedita quas ex est! Nulla dignissimos nam quam natus ab in veniam quaerat doloremque maxime molestias aperiam, libero, omnis, facere nesciunt.
					</p>
					<a href="#" class="uk-button uk-button-default">
						GO!
					</a>
				</div>
			</div>

			<div>
				<div class="uk-card uk-card-body">
					<div>
						<i class="fab fa-js fa-7x"></i>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, expedita quas ex est! Nulla dignissimos nam quam natus ab in veniam quaerat doloremque maxime molestias aperiam, libero, omnis, facere nesciunt.
					</p>
					<a href="#" class="uk-button uk-button-default">
						GO!
					</a>
				</div>
			</div>

		</div>
	</div>
</section>