@extends('templates.default')

@section('content')
<section class="uk-section">
    <div class="uk-grid" uk-grid>

        <aside class="uk-width-1-4@s">
            @include('templates.partials.account_nav')
        </aside>

        <main class="uk-width-3-4@s">

            <h1>Manage your subscription</h1>

            @if(Auth::user()->subscription('main')->cancelled())
                <p>
                    You have cancelled your subscription. 
                    You still have access until {{Auth::user()->subscription('main')->ends_at->format('dS M Y')}}
                </p>
                <form action="{{ route('subscription.resume') }}" method="post">
                    @csrf 
                    <button type="submit" class="uk-button uk-button-primary">Resume Subscription</button>
                </form>
            @else
                <p>You are currently subscribed.</p>
                <form action="{{route('subscription.cancel')}}" method="post">
                    @csrf 
                    <button type="submit" class="uk-button uk-button-danger">Cancel Subscription</button>
                </form>
            @endif
        </main>

    </div>
</section>
@endsection