<div id="profile-edit" uk-modal>
    <div class="uk-modal-dialog">

        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form class="uk-form-horizontal" method="POST" action="{{route('profile.edit',$user->username)}}">
            @csrf
            <div class="uk-modal-header">
                <h2 class="uk-modal-title">Edit your profile</h2>
            </div>

            <div class="uk-modal-body" uk-overflow-auto>
                
                <div class="uk-margin">
                    <label for="first_name">First Name</label>
                    <div class="uk-inline uk-width-1-1">
                        <input 
                            type="text" 
                            name="first_name"
                            id="first_name"
                            class="uk-input {{$errors->has('first_name') ? 'uk-form-danger' : ''}}"
                            value="{{$user->first_name}}"
                            placeholder="Your first name" 
                        >
                    </div>
                    @if ($errors->has('first_name'))
                        <small class="uk-text-danger">{{ $errors->first('first_name') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="last_name">Last Name</label>
                    <div class="uk-inline uk-width-1-1">
                        <input 
                            type="text" 
                            name="last_name"
                            id="last_name"
                            class="uk-input {{$errors->has('last_name') ? 'uk-form-danger' : ''}}"
                            value="{{$user->last_name}}"
                            placeholder="Your last name" 
                        >
                    </div>
                    @if ($errors->has('last_name'))
                        <small class="uk-text-danger">{{ $errors->first('last_name') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="bio">Bio</label>
                    <textarea
                        name="bio"
                        id="bio"
                        class="uk-textarea {{$errors->has('bio') ? 'uk-form-danger' : ''}}"
                        placeholder="Tell others something about yourself, or just paste in some kind inspirational quote." 
                    >{{$user->getBio()}}</textarea>

                    @if ($errors->has('bio'))
                        <small class="uk-text-danger">{{ $errors->first('bio') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="twitter">Twitter</label>
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon"><i class="fab fa-twitter"></i></span>
                        <input 
                            type="text" 
                            name="twitter"
                            id="twitter"
                            class="uk-input {{$errors->has('twitter') ? 'uk-form-danger' : ''}}"
                            value="{{$user->twitter}}"
                            placeholder="The link to your twitter profile" 
                        >
                    </div>
                    @if ($errors->has('twitter'))
                        <small class="uk-text-danger">{{ $errors->first('twitter') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="facebook">Facebook</label>
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon"><i class="fab fa-facebook"></i></span>
                        <input 
                            type="text" 
                            name="facebook"
                            id="facebook"
                            class="uk-input {{$errors->has('facebook') ? 'uk-form-danger' : ''}}" 
                            value="{{$user->facebook}}"
                            placeholder="The link to your facebook profile" 
                        >
                    </div>
                    @if ($errors->has('facebook'))
                        <small class="uk-text-danger">{{ $errors->first('facebook') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="youtube">YouTube</label>
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon"><i class="fab fa-youtube"></i></span>
                        <input 
                            type="text" 
                            name="youtube"
                            id="youtube"
                            class="uk-input {{$errors->has('youtube') ? 'uk-form-danger' : ''}}" 
                            value="{{$user->youtube}}"
                            placeholder="The link to your YouTube channel" 
                        >
                    </div>
                    @if ($errors->has('youtube'))
                        <small class="uk-text-danger">{{ $errors->first('youtube') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="github">GitHub</label>
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon"><i class="fab fa-github"></i></span>
                        <input 
                            type="text" 
                            name="github"
                            id="github"
                            class="uk-input {{$errors->has('github') ? 'uk-form-danger' : ''}}" 
                            value="{{$user->github}}"
                            placeholder="The link to your Github profile" 
                        >
                    </div>
                    @if ($errors->has('github'))
                        <small class="uk-text-danger">{{ $errors->first('github') }}</small>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="bitbucket">Bitbucket</label>
                    <div class="uk-inline uk-width-1-1">
                        <span class="uk-form-icon"><i class="fab fa-bitbucket"></i></span>
                        <input 
                            type="text" 
                            name="bitbucket"
                            id="bitbucket"
                            class="uk-input {{$errors->has('bitbucket') ? 'uk-form-danger' : ''}}" 
                            value="{{$user->bitbucket}}"
                            placeholder="The link to your Bitbucket profile" 
                        >
                    </div>
                    @if ($errors->has('bitbucket'))
                        <small class="uk-text-danger">{{ $errors->first('bitbucket') }}</small>
                    @endif
                </div>

            </div>

            <div class="uk-modal-footer uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="submit">Save</button>
            </div>
        </form>

    </div>
</div>