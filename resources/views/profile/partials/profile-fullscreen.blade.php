<div class="uk-background-cover uk-height-medium uk-flex uk-flex-bottom uk-light tm-height-small tm-profile-banner tm-position-relative" 
	style="background-image: url({{$user->getProfileBanner()}});">
	@if(Auth::user() == $user)
		<a href="#banner-upload" class="tm-edit-button" uk-toggle><i class="fas fa-camera uk-margin-small-right"></i>Edit</a>
	@endif
	<div class="uk-container tm-profile-content">
		<div class="uk-flex-bottom uk-flex-center uk-flex-left@m" uk-grid>
			<div class="uk-width-1-5@s uk-width-1-6@m uk-width-small">
				<div class="uk-inline tm-profile-pic-container tm-position-relative">
			        <img src="{{$user->getProfilePicture()}}" width="200" class="tm-profile-pic uk-border-rounded">
			        @if(Auth::user() == $user)
			        	<a href="#profile-pic-upload" class="tm-edit-button" uk-toggle><i class="fas fa-camera uk-margin-small-right"></i>Edit</a>
			        @endif
			    </div>
		    </div>
		    <div class="uk-width-4-5@s uk-width-5-6@m uk-visible@s tm-margin-bottom-40">
		        <h3>{{$user->getNameOrUsername()}}</h3>
		    </div>
		</div>
	</div>
</div>
<div class="tm-border-bottom uk-margin-top">
	<div class="uk-container uk-hidden@s uk-text-center uk-padding-small">
		<h3>{{$user->getNameOrUsername()}}</h3>
	</div>
	<div class="uk-container">
		<div class="uk-text-center">
			{{$user->getBio()}}
		</div>
		<div class="uk-margin uk-margin-bottom">
			<div class="uk-text-center">
				@if($user->twitter)
					<a href="https://{{$user->twitter}}" class="uk-link-reset uk-margin-right" target="_blank">
						<i class="fab fa-twitter fa-lg"></i>
					</a>
				@endif
				@if($user->facebook)
					<a href="https://{{$user->facebook}}" class="uk-link-reset uk-margin-right" target="_blank">
						<i class="fab fa-facebook fa-lg"></i>
					</a>
				@endif
				@if($user->youtube)
					<a href="https://{{$user->youtube}}" class="uk-link-reset uk-margin-right" target="_blank">
						<i class="fab fa-youtube fa-lg"></i>
					</a>
				@endif
				@if($user->github)
					<a href="https://{{$user->github}}" class="uk-link-reset uk-margin-right" target="_blank">
						<i class="fab fa-github fa-lg"></i>
					</a>
				@endif
				@if($user->bitbucket)
					<a href="https://{{$user->bitbucket}}" class="uk-link-reset uk-margin-right" target="_blank">
						<i class="fab fa-bitbucket fa-lg"></i>
					</a>
				@endif
			</div>
		</div>
		@if(Auth::user() == $user)
		<div class="uk-margin-bottom">
		    <div class="uk-text-center">
		    	<a class="uk-button uk-button-link" href="#profile-edit" uk-toggle>Edit Profile</a>
		    </div>
		</div>
		@endif
	</div>
</div>
	@if(Auth::user() == $user)
		@include('profile.partials.edit-modal')
		@include('profile.partials.profile-pic-upload')
		@include('profile.partials.banner-upload')
	@endif