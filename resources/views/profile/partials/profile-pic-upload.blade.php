@if($errors->has('profile'))
    <div id="profile-pic-upload" uk-modal class="uk-open" style="display: block;">
@else
    <div id="profile-pic-upload" uk-modal>
@endif
    <div class="uk-modal-dialog">

        <button class="uk-modal-close-default" type="button" uk-close></button>
        <form class="uk-form-horizontal" method="POST" action="{{route('profile.pic',$user->username)}}" enctype="multipart/form-data">
            @csrf
            <div class="uk-modal-header">
                <h2 class="uk-modal-title">Upload a profile picture</h2>
            </div>

            <div class="uk-modal-body" uk-overflow-auto>

                <div class="uk-margin" uk-margin>
                    <div uk-form-custom="target: true">
                        <input type="file" name="profile">
                        <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
                        <button class="uk-button uk-button-default" type="button" tabindex="-1">Select</button>
                    </div>
                </div>
                @if($errors->has('profile'))
                    <small class="uk-text-danger">{{$errors->first('profile')}}</small>
                @endif

                <div class="uk-margin" uk-margin>
                    <input type="submit" value="Upload" class="uk-button uk-button-primary">
                </div>

			</div>
        </form>

    </div>
</div>