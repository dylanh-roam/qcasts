<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('templates.partials.head')
<body>
    
    <div id="app" class="uk-offcanvas-content">
        @include('templates.partials.header')
        @yield('fullscreen')
        <div id="tm-content">
            <div class="uk-container">
                @include('templates.partials.alerts')
                @yield('content')
            </div>
        </div>

        @include('templates.partials.footer')
    </div>
    @include('templates.partials.offcanvas-menu')

    @yield('scripts.footer')
   
    <script type="text/javascript" src="{{asset('uikit/js/uikit-icons.min.js')}}"></script>

</body>
</html>
