<div class="uk-card uk-card-default">
	<div class="uk-card-header">
		<h3>More options</h3>
	</div>
	<div class="uk-card-body">
		<ul class="uk-nav">
			<li>
				<a href="{{route('subscription')}}">
					Subscription
				</a>
			</li>
			<li>
				<a href="{{route('plans')}}">
					Change plan
				</a>
			</li>
			<li>
				<a href="{{route('invoices')}}">
					Invoices
				</a>
			</li>
		</ul>
	</div>
</div>