<div id="offcanvas-push" uk-offcanvas="mode: push; overlay: true;">
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <ul class="uk-nav uk-nav-default">
        	<li class="uk-nav-header">
	        	General
			</li>
			<li>
                <a href="{{route('home')}}">
                    <i class="fas fa-home uk-margin-small-right"></i>Home
                </a>
                <li>
	                <a href="{{route('plans')}}">
	                    Plans
	                </a>
	            </li>
	            <li>
	                <a href="{{route('lessons')}}">
	                    Lessons
	                </a>
	            </li>
	            <li>
	                <a href="{{route('lessons.pro')}}">
	                    Pro Lessons
	                </a>
	            </li>
            </li>
		@guest
			<li class="uk-nav-header">
	        	Account
			</li>
			<li>
                <a href="{{route('login')}}">
                	<i class="fas fa-sign-in-alt uk-margin-small-right"></i>Login
                </a>
            </li>
            <li>
                <a href="{{route('register')}}">
                	<i class="fas fa-user-plus uk-margin-small-right"></i>Sign Up
                </a>
            </li>
		@else
        
	        <li class="uk-nav-header">
	        	<img src="{{asset(Auth::user()->getProfilePicture())}}" class="uk-border-circle uk-margin-small-right" width="35">
	        	{{Auth::user()->getNameOrUsername()}}
			</li>
	        <li>
	        	<a href="{{route('profile',Auth::user()->username)}}">
	        		<i class="fas fa-user uk-margin-small-right"></i> My Profile
	        	</a>
	        </li>
	        <li>
	        	<a href="{{route('subscription')}}">
	        		<i class="fas fa-credit-card uk-margin-small-right"></i>My Subscription
	        	</a>
	        </li>
	        <li>
	        	<a class="dropdown-item" href="{{ route('logout') }}"
				   onclick="event.preventDefault();
				   document.getElementById('logout-form').submit();">
				   <i class="fas fa-sign-out-alt uk-margin-small-right"></i>
				   Logout
				</a>
				
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				    @csrf
				</form>
			</li>
		@endguest
	    </ul>
    </div>
</div>