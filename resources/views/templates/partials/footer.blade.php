<footer id="tm-footer" class="uk-section uk-background-dark">
	<div class="uk-container uk-text-center">
		<p>
			<ul class="uk-list">
				<li class="uk-display-inline-block uk-margin-small-right">
					<a href="https://twitter.com/QuentinWatt" class="uk-link-reset" target="_blank">
						<i class="fab fa-twitter fa-lg"></i>
					</a>
				</li>
				<li class="uk-display-inline-block uk-margin-small-right">
					<a href="https://facebook.com/QuentinWatt" class="uk-link-reset" target="_blank">
						<i class="fab fa-facebook fa-lg"></i>
					</a>
				</li>
				<li class="uk-display-inline-block uk-margin-small-right">
					<a href="https://youtube.com/QuentinWatt" class="uk-link-reset" target="_blank">
						<i class="fab fa-youtube fa-lg"></i>
					</a>
				</li>
				<li class="uk-display-inline-block uk-margin-small-right">
					<a href="https://github.com/QuentinWatt" class="uk-link-reset" target="_blank">
						<i class="fab fa-github fa-lg"></i>
					</a>
				</li>
				<li class="uk-display-inline-block uk-margin-small-right">
					<a href="https://bitbucket.com/QuentinWatt" class="uk-link-reset" target="_blank">
						<i class="fab fa-bitbucket fa-lg"></i>
					</a>
				</li>
			</ul>
		</p>
		<p><small>&copy; {{ date("Y") }} Qcasts Limited. All rights reserved.</small></p>
	</div>
</footer>