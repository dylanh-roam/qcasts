<a>
	<img src="{{asset(Auth::user()->getProfilePicture())}}" class="uk-border-circle uk-margin-small-right" width="35">
	My Account <i class="uk-margin-small-left fas fa-caret-square-down"></i>
</a>
<div class="uk-navbar-dropdown" uk-dropdown="pos: top-right; mode: click">
    <ul class="uk-nav uk-navbar-dropdown-nav">
        <li>
        	<a href="{{route('profile',Auth::user()->username)}}">
        		<i class="fas fa-user uk-margin-small-right"></i>Profile
        	</a>
        </li>
        <li>
        	<a href="{{route('subscription')}}">
        		<i class="fas fa-credit-card uk-margin-small-right"></i>Subscription
        	</a>
        </li>
        <li>
        	<a class="dropdown-item" href="{{ route('logout') }}"
			   onclick="event.preventDefault();
			   document.getElementById('logout-form').submit();">
			   <i class="fas fa-sign-out-alt uk-margin-small-right"></i>
			   Logout
			</a>
			
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			    @csrf
			</form>
		</li>
    </ul>
</div>