<div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; top: 200" class="uk-background-dark">

    <nav class="uk-navbar-container">
        <div class="uk-container">
            <div uk-navbar="mode: click; dropbar: true;">

                <div class="uk-navbar-left">
			        <a class="uk-navbar-item uk-logo" href="{{route('home')}}">
						<img src="{{asset('/images/qcasts-logo.png')}}" alt="qcasts" width="150">
			        </a>
					<ul class="uk-navbar-nav uk-visible@m">
						<li>
			                <li>
				                <a href="{{route('plans')}}">
				                    Plans
				                </a>
				            </li>
				            <li>
				                <a href="{{route('lessons')}}">
				                    Lessons
				                </a>
				            </li>
				            <li>
				                <a href="{{route('lessons.pro')}}">
				                    Pro Lessons
				                </a>
				            </li>
			            </li>
			        </ul>
			    </div>

			    <div class="uk-navbar-right">
			    	<ul class="uk-navbar-nav uk-visible@m">
			    		@guest
			            <li>
			                <a href="{{route('login')}}">
			                    Login
			                </a>
			            </li>
			            <li>
			                <a href="{{route('register')}}">
			                    Sign Up
			                </a>
			            </li>
						@else
			            <li>
							@include('templates.partials.login-profile')
			            </li>
			            @endguest
			        </ul>
			        <a class="uk-navbar-toggle uk-hidden@m" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-push"></a>
			    </div>

            </div>
        </div>
    </nav>

</div>