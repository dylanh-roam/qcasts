<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBannerLocationColumnsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('banner_url')->nullable()->after('profile_image_url');
            $table->text('bio')->nullable()->after('banner_url');
            $table->string('twitter')->nullable()->after('banner_url');
            $table->string('youtube')->nullable()->after('banner_url');
            $table->string('facebook')->nullable()->after('banner_url');
            $table->string('github')->nullable()->after('banner_url');
            $table->string('bitbucket')->nullable()->after('banner_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'banner_url',
                'twitter',
                'youtube',
                'facebook',
                'github',
                'bitbucket',
            ]);
        });
    }
}
